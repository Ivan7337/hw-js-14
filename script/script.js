const tabs = document.querySelector('.tabs')
const tabsContent = document.querySelector('.tabs-content')
const activeTab = document.querySelector('.tabs-content .active')

const showContentActiveTab = () => {
    tabsContent.querySelectorAll('.tab').forEach(content => {
        content.style.display = 'none'
    })
    activeTab.style.display = 'block'
}

const showTabWithContent = (event) => {
    const clickedTab = event.target.closest('.tabs-title')
    if (!clickedTab) {
		return
	}
    const index = Array.from(tabs.children).indexOf(clickedTab)

    tabs.querySelectorAll('.tabs-title').forEach(tab => {
        tab.classList.remove('active')
    })
    clickedTab.classList.add('active')

    tabsContent.querySelectorAll('.tab').forEach(content => {
        content.style.display = 'none'
    })
    tabsContent.querySelectorAll('.tab')[index].style.display = 'block'
}

tabs.addEventListener('click', showTabWithContent)

showContentActiveTab()